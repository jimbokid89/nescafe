'use strict'

var used = 0;
var changeNum = 1;
var activeRows = 0;
var modifierSum = 0;
var modifierScope = 0;
var disableNextBtn = false;
var checks = false;

$(document).on('click', 'input[type=checkbox]', function () {
    var test = 0;
    var lenght = $('.table-row').length;


    $('.table-row.disabled').each(function () {
        test += parseInt($(this).closest('.table-row').find('.count-drinks span:first').html());
    })
    activeRows = lenght - $('.table-row.disabled').length;

    console.log(activeRows);


    $('.table-row.disabled').length;
    modifierScope = Math.round(test / activeRows);
    $('#Test').val(modifierScope)
    $('#Test').trigger('input');


});

function preloader(status) {
    if (status == 'show') {
        $('.preloader').fadeIn();
    } else if (status == 'hide') {
        $('.preloader').fadeOut();
        closePop()
    }
}

function showPopUpMsg(response) {
    var popMsg = $('.pop-up.message');
    popMsg.fadeIn();
    if (response == 'good') {
        popMsg.find('.pop-msg').html('Ваша заявка отправлена');

    } else if (response == 'error') {
        popMsg.find('.pop-msg').html('Произошла ошибка');
    }
    popMsg.css({
        'top': $(window).scrollTop() + 100
    })
}


var app = angular.module('App', []);

app.controller('calcController', function ($scope, $http) {
    $scope.showState = 'step1';
    $scope.tabState = 'people';
    $scope.activeRow = true;
    $scope.targetTable = {};
    $scope.customPrice = 0;
    $scope.drinksCount = 0;
    $scope.peopleCoef = 2.5;
    $scope.modifier = 0;
    $scope.tableSize = 0;
    $scope.sliderValue = 0;
    $scope.sliderValuePeople = 0;
    $scope.sliderValueBudget = 0;
    $scope.profileName = '';
    $scope.profiles = [];
    $scope.enabledCheck = false;

    $scope.tableChk = function () {

        console.log($scope.enabledCheck);
        console.log(activeRows);


        if ($('.table-row.enabled').length < 5) {
            // checks = true;
            console.log('выключить')
            $scope.enabledCheck = true;
        } else {
            // checks = false;
            console.log('включить')
            $scope.enabledCheck = false;
        }

        // $('#checks').val(checks)
        // $('#checks').trigger('input');

        if ($('.table-row.disabled').length === $('.table-row').length) {
            $scope.disableNextBtn = true;
        } else {
            $scope.disableNextBtn = false;
        }
        $scope.activeRow = true;
    }

    $scope.setChecks = function () {
        $('.table-row .check-row input').prop('checked', true);
        $.each($scope.hotelPlaceTable.hotelPlaces, function (key, value) {
            value.status = true;
        });
        $.each($scope.targetTable.drinks, function (key, value) {
            value.status = true;
            $scope.modifier = 0;
        });
        $scope.activeRow = true;
    }


    $scope.hotelPlaceTable = {
        'hotelPlaces': {
            'zavtrak': {
                'placeName': 'завтрака/шведского стол',
                'machine': 'Nescafe Alegria 10/60',
                'status': true,
                'budget': 41000,
                'art': '2514/302',
                'img': 'NescafeAlegria10.60.jpg'
            },
            'conference': {
                'placeName': 'конференц-зала',
                'machine': 'Nescafe Lounge',
                'status': true,
                'budget': 28000,
                'art': '2514/302',
                'img': 'NescafeLounge.jpg'
            },
            'personal': {
                'placeName': 'комната персонала',
                'machine': 'Nescafe Alegria 6/30',
                'status': true,
                'budget': 8500,
                'art': '2514/302',
                'img': 'NescafeAlegria6.30.jpg'
            },
            'service': {
                'placeName': 'room service',
                'machine': 'Nescafe Milano 2',
                'status': true,
                'budget': 28000,
                'art': '2514/302',
                'img': 'NescafeMilano.jpg'
            },
            'lobby': {
                'placeName': 'лобби-бара',
                'machine': 'Astoria Calypso',
                'status': true,
                'budget': 40000,
                'art': '2514/302',
                'img': 'NescafeAlegriaCalypso.jpg'
            }
        }
    };

    //Черный кофе
    $scope.black_coffee = {
        'drinkName': 'Черный кофе',
        'drinkCoef': 0.25,
        'drinkPrice': 50,
        'customPrice': 50,
        'cost_price': 9.26,
        'status': true,
    };
    //Кофе с молоком
    $scope.coffee_milk = {
        'drinkName': 'Кофе с молоком',
        'drinkCoef': 0.20,
        'drinkPrice': 80,
        'customPrice': 80,
        'cost_price': 13.22,
        'status': true,
    };
    //Капучино
    $scope.capucino = {
        'drinkName': 'Капучино',
        'drinkCoef': 0.37,
        'drinkPrice': 160,
        'customPrice': 160,
        'cost_price': 25.65,
        'status': true,
    };
    //Большой Капучино
    $scope.big_capucion = {
        'drinkName': 'Большой Капучино ',
        'drinkCoef': 0.2,
        'drinkPrice': 180,
        'customPrice': 180,
        'cost_price': 25.65,
        'status': true,
    };

    //Ванильный Капучино
    $scope.vanil_capucino = {
        'drinkName': 'Ванильный Капучино',
        'drinkCoef': 0.17,
        'drinkPrice': 100,
        'customPrice': 100,
        'cost_price': 16.63,
        'status': true,
    };

    //Большой ванильный Капучино
    $scope.big_vanil_capucino = {
        'drinkName': 'Большой ванильный Капучино',
        'drinkCoef': 0.15,
        'drinkPrice': 120,
        'customPrice': 120,
        'cost_price': 25.65,
        'status': true,
    };


    //Латте
    $scope.latte = {
        'drinkName': 'Латте',
        'drinkCoef': 0.18,
        'drinkPrice': 80,
        'customPrice': 80,
        'cost_price': 13.22,
        'status': true,
    };
    //Большой Латте
    $scope.big_latte = {
        'drinkName': 'Большой Латте',
        'drinkCoef': 0.10,
        'drinkPrice': 100,
        'customPrice': 100,
        'cost_price': 13.22,
        'status': true,
    };
    //Эспрессо
    $scope.espresso = {
        'drinkName': 'Эспрессо',
        'drinkCoef': 0.18,
        'drinkPrice': 60,
        'customPrice': 60,
        'cost_price': 9.26,
        'status': true,
    };
    $scope.americano = {
        'drinkName': 'Американо',
        'drinkCoef': 0.2,
        'drinkPrice': 80,
        'customPrice': 80,
        'cost_price': 12.67,
        'status': true,
    };
    //Шоколад
    $scope.chockolade = {
        'drinkName': 'Шоколад',
        'drinkCoef': 0.15,
        'drinkPrice': 115,
        'customPrice': 115,
        'cost_price': 19.19,
        'status': true,
    };
    //Мокачино
    $scope.mockachino = {
        'drinkName': 'Мокачино',
        'drinkCoef': 0.09,
        'drinkPrice': 140,
        'customPrice': 140,
        'cost_price': 22.72,
        'status': true,
    }


    $scope.machine_na_4_30 = {
        'name': 'NESCAFE Alegria 4/30',
        'maxDrinks': 20,
        'img': 'calcNescafeAlegria4.30.jpg',
        'drinks': [
            $scope.black_coffee,
            $scope.coffee_milk,
            $scope.capucino,
            $scope.latte,
        ]
    };
//****************************************************
    $scope.machine_na_6_30 = {
        'name': 'NESCAFE Alegria 6/30',
        'maxDrinks': 30,
        'img': 'calcNescafeAlegria6.30.jpg',
        'drinks': [
            $scope.espresso,
            $scope.capucino,
            $scope.big_capucion,
            $scope.latte,
            $scope.chockolade,
            $scope.vanil_capucino
        ]
    };

    $scope.machine_na_8_40 = {
        'name': 'NESCAFE Alegria 8/40',
        'maxDrinks': 40,
        'img': 'calcNescafeAlegria8.40.jpg',
        'drinks': [
            $scope.espresso,
            $scope.americano,
            $scope.capucino,
            $scope.big_capucion,
            $scope.latte,
            $scope.big_latte,
            $scope.chockolade,
            $scope.vanil_capucino,
        ]
    };

    $scope.machine_na_a_1060 = {
        'name': 'NESCAFE Alegria 1060',
        'maxDrinks': 150,
        'img': 'calcNescafeAlegria10.60.jpg',
        'drinks': [
            $scope.espresso,
            $scope.americano,
            $scope.capucino,
            $scope.big_capucion,
            $scope.latte,
            $scope.big_latte,
            $scope.chockolade,
            $scope.big_vanil_capucino,
            $scope.mockachino,
            $scope.vanil_capucino,
        ]
    };

    $scope.machine_na_milano = {
        'name': 'Nescafe Milano 2',
        'maxDrinks': 100,
        'img': 'calcNescafeMilano.jpg',
        'drinks': [
            $scope.espresso,
            $scope.americano,
            $scope.capucino,
            $scope.big_capucion,
            $scope.latte,
            $scope.big_latte,
            $scope.chockolade,
            $scope.big_vanil_capucino,
            $scope.mockachino,
            $scope.vanil_capucino,
        ]
    };

    $scope.machine_na_lounge = {
        'name': 'Nescafe Lounge',
        'maxDrinks': 100,
        'drinks': [
            $scope.espresso,
            $scope.americano,
            $scope.capucino,
            $scope.big_capucion,
            $scope.latte,
            $scope.big_latte,
            $scope.chockolade,
            $scope.big_vanil_capucino,
            $scope.mockachino,
            $scope.vanil_capucino,
        ]
    };

    $scope.machine_a_gemma = {
        'name': 'Astoria Gemma',
        'maxDrinks': 100,
        'img': 'calcAstoriaGemma.jpg',
        'drinks': [
            $scope.espresso,
            $scope.americano,
            $scope.capucino,
            $scope.big_capucion,
            $scope.latte,
            $scope.big_latte,
        ]
    };

    $scope.machine_a_calypso = {
        'name': 'Astoria Calypso',
        'maxDrinks': 100,
        'img': 'calcAstoriaCalypso.jpg',
        'drinks': [
            $scope.espresso,
            $scope.americano,
            $scope.capucino,
            $scope.big_capucion,
            $scope.latte,
            $scope.big_latte,
        ]
    };


    $scope.getTotal = function (targetVal) {
        $scope.total = 0;
        $scope.total_marginal = 0;
        $scope.cups = 0;
        $scope.targetTable.drinks.drinksCount;
        console.log(targetVal);
        $scope.tableSize = $scope.targetTable.drinks.length;
        var countDrinks = [];
        $('.table-row').each(function () {
            countDrinks.push(parseInt($(this).find('.count-drinks .enabled').html()));
        });

        console.log(countDrinks)

        for (var i = 0; i < $scope.targetTable.drinks.length; i++) {
            if ($scope.targetTable.drinks[i].status == false) {

            } else {
                console.log($scope.targetTable.drinks[i])
                var row = parseInt($scope.targetTable.drinks[i].customPrice) * countDrinks[i];
                var marginal = parseInt($scope.targetTable.drinks[i].cost_price) * countDrinks[i];
                $scope.total += row;
                $scope.total_marginal += marginal;
                $scope.cups++;
                console.log(row)
            }
        }
        console.log($scope.cups);
        console.log('_____________')
        console.log($scope.total);
        console.log($scope.total_marginal);
    }

    $scope.remove = function (item) {
        item.status = !item.status;
    }

///**************************************************************
// $scope.targetTable = $scope.machine_na_4_30;
// $scope.getTotal();

    $scope.hotelBtn = 'следующий шаг'

    $scope.nextStep = function () {
        if ($scope.showState == 'step1') {
            //console.log($scope.sliderValue);
            //console.log($scope.tabState);
            $scope.hotelBtn = 'следующий шаг'

            //Подбор таблицы и машины
            if ($scope.tabState == 'people') {
                $scope.sliderValue = $scope.sliderValuePeople;
                switch (true) {
                    case ($scope.sliderValue <= 20):
                        console.log('NA 4/30');
                        $scope.targetTable = null;
                        $scope.targetTable = $scope.machine_na_4_30;
                        break;
                    case $scope.sliderValue > 20 && $scope.sliderValue <= 30:
                        console.log('NA 6/30');
                        $scope.targetTable = null;
                        $scope.targetTable = $scope.machine_na_6_30;
                        break;
                    case $scope.sliderValue > 30 && $scope.sliderValue <= 40:
                        console.log('NA 8/40');
                        $scope.targetTable = $scope.machine_na_8_40;
                        break;
                    case $scope.sliderValue > 40 && $scope.sliderValue <= 150:
                        console.log('NA A 1060');
                        $scope.targetTable = $scope.machine_na_a_1060;
                        break;
                }
            } else if ($scope.tabState == 'budget') {
                console.log('budget');
                $scope.sliderValue = $scope.sliderValueBudget;
                switch (true) {
                    case ($scope.sliderValue <= 5500):
                        console.log('NA 4/30');
                        $scope.targetTable = null;
                        $scope.targetTable = $scope.machine_na_4_30;
                        break;
                    case $scope.sliderValue > 5500 && $scope.sliderValue <= 8500:
                        console.log('NA 6/30');
                        $scope.targetTable = null;
                        $scope.targetTable = $scope.machine_na_6_30;
                        break;
                    case $scope.sliderValue > 8500 && $scope.sliderValue <= 11000:
                        console.log('NA 8/40');
                        $scope.targetTable = $scope.machine_na_8_40;
                        break;
                    case $scope.sliderValue > 11000 && $scope.sliderValue <= 41000:
                        console.log('NA A 1060');
                        $scope.targetTable = $scope.machine_na_a_1060;
                        break;
                }
            }
            else if ($scope.tabState == 'cafe') {
                switch (true) {
                    case ($scope.sliderValue <= 20):
                        console.log('NA 4/30');
                        $scope.targetTable = null;
                        $scope.targetTable = $scope.machine_na_4_30;
                        break;
                    case $scope.sliderValue > 20 && $scope.sliderValue <= 30:
                        console.log('NA 6/30');
                        $scope.targetTable = null;
                        $scope.targetTable = $scope.machine_na_6_30;
                        break;
                    case $scope.sliderValue > 30 && $scope.sliderValue <= 40:
                        console.log('NA 8/40');
                        $scope.targetTable = $scope.machine_na_8_40;
                        break;
                    case $scope.sliderValue > 40 && $scope.sliderValue <= 150:
                        console.log('NA A 1060');
                        $scope.targetTable = $scope.machine_na_a_1060;
                        break;
                }
            }


            $scope.showState = 'step2'

            $scope.getProfileMainId();

            setTimeout(function () {
                activeRows = $('.table-row').length


                if (activeRows <= 4) {
                    $scope.enabledCheck = true;
                } else {
                    $scope.enabledCheck = false;
                }
                $('#checks').val($scope.enabledCheck);
                $('#checks').trigger('input');
                console.log($scope.enabledCheck);
            }, 300);


        } else if ($scope.showState == 'step2') {
            if ($scope.tabState == 'people') {
                $scope.getTotal($scope.sliderValue);
            } else if ($scope.tabState == 'budget') {
                $scope.getTotal($scope.targetTable.maxDrinks);
            } else if ($scope.tabState == 'hotel') {
                console.log('hotel');



            } else if ($scope.tabState == 'cafe') {
                $scope.getTotal($scope.sliderValue);
            }

            $('.offer-img').attr('src','/frontend/web/'+ $('.hidden-img').attr('ng-src'));



            $scope.showState = 'step3';
            $scope.hotelBtn = 'Оформить заказ';

            $('.hotel-offer-row').each(function(){
                var src = $(this).find('.machine-img .hidden-img').attr('ng-src');
                $(this).find('.machine-img .offer-img').attr('src','/frontend/web/'+ src);
            });
        } else if ($scope.showState == 'step3') {
            $('.calc-form').submit();
        }
    }

    $scope.prevStep = function () {
        if ($scope.showState == 'step3') {
            $scope.showState = 'step2';
            $scope.hotelBtn = 'перейти к следующиму шагу';
        } else if ($scope.showState == 'step2') {
            $scope.setChecks();
            $scope.disableNextBtn = false;
            $scope.activeRow = true;
            $scope.enabledCheck = false;
            $scope.showState = 'step1';
            $scope.hotelBtn = 'перейти к следующиму шагу';
        } else if ($scope.showState == 'step1') {
            location.href = '../site/find-solution';
        }

    }

    $scope.formDataCalcOffer = {
        "profile_id": 0,
        "country": "",
        "city": "",
        "firstname": "",
        "lastname": "",
        "phone": "",
        "email": ""
    }


    $scope.getProfileMainId = function () {
        function getProfilesFn() {
            $http.get('/api/profiles/index')
                .success(function (data) {
                    $scope.profiles = data;
                    console.log($scope.profiles);
                    getProfileIdFn();
                })
                .error(function (data, status) {
                    console.log(data);
                });
        }

        getProfilesFn()

        function getProfileIdFn() {
            $.each($scope.profiles, function (key, value) {
                if (value.name == $scope.profileName) {
                    $scope.formDataCalcOffer.profile_id = value.id;
                }
            })
        }

    };


    $scope.getCalcValues = function () {

        console.log('Call Function!');
        $scope.profiles = [];


        $scope.formDataCalcOffer.machine = $scope.targetTable.name
        console.log($scope.targetTable.drinks);
        var string = '';
        var cups = [];
        $('.table-row').each(function () {
            cups.push($(this).find('.count-drinks span:last').html())
        });

        console.log(cups);

        $.each($scope.targetTable.drinks, function (value, key) {
            if (key.status == true || key.status == 'true') {
                string += 'Напиток - ' + key.drinkName + ', рекомендуемая цена продажи - ' + key.drinkPrice + ', цена продажи - ' + key.customPrice + ', количество чашек - ' + cups[value] + '; \n'
            }
        });

        $scope.formDataCalcOffer.calculation_option = $('.slider-range-wrap .guests').html() + ': ' + $scope.sliderValue;
        $scope.formDataCalcOffer.expenses = 0;
        $scope.formDataCalcOffer.income = $scope.cups * $scope.total * 31;

        $scope.formDataCalcOffer.additional_information = string;

        console.log($scope.formDataCalcOffer);
    }

    $scope.show = function () {
        console.log($scope.enabledCheck)
    }

    $scope.sendCalcOffer = function () {
        if ($('.calc-form').valid()) {

            $scope.formDataCalcOffer.country = $('.calc-form select[name=country]').val();

            $scope.formDataCalcOffer.city = $('.calc-form select[name=town]').val();

            $scope.getCalcValues();
            preloader('show');
            console.log($scope.formDataCalcOffer);
            $http({
                method: 'POST',
                url: '/api/offer/create',
                data: $scope.formDataCalcOffer
            })
                .then(function (response) {
                    console.log(response);
                    setTimeout(function () {
                        preloader('hide');
                        showPopUpMsg('good');
                    }, 1000)

                })
                .catch(function (err) {
                    console.log(err);

                    setTimeout(function () {
                        preloader('hide');
                        showPopUpMsg('error');
                    }, 1000)
                });

        }
    }
})
;

var count = 200;

app.controller('offerController', function ($scope, $http) {
    $scope.profiles = [];

    $http.get('/api/profiles/index')
        .success(function (data) {
            $scope.profiles = data;
            setTimeout(function(){
                $('select[name=profile]').trigger('refresh');
            },100)
        })
        .error(function (data, status) {
            console.log(data);
        });


    // Получения профилей через фабрику, если не нужно можно потом удалить

    // $scope.profiles = [];
    //
    // var handleSuccess = function(data, status) {
    //     $scope.profiles = data;
    //     console.log($scope.profiles);
    // };
    //
    // getProfile.getProfiles().success(handleSuccess);


    $scope.formData = {
        "profile": "",
        "city": "",
        "count_portion": count,
        "firstname": "",
        "lastname": "",
        "phone": "",
        "email": ""
    }
    $scope.sendOffer = function () {

        $scope.formData.profile = $('.offer-form select[name=profile]').val();
        $scope.formData.city = $('.offer-form select[name=town]').val();

        $scope.formData.count_portion = count;
        if ($('.offer-form').valid()) {
            preloader('show');
            console.log($scope.formData);
            $http({
                method: 'POST',
                url: '/api/order/create',
                data: $scope.formData
            })
                .then(function (response) {
                    console.log(response);
                    setTimeout(function () {
                        preloader('hide');
                        showPopUpMsg('good');
                    }, 1000)

                })
                .catch(function (err) {
                    console.log(err);

                    setTimeout(function () {
                        preloader('hide');
                        showPopUpMsg('error');
                    }, 1000)
                });

        }
    }


});


app.directive('myCustomer', function () {
    return {
        scope: true,
        template: '<input type="number" class="number-styler" min="15" max="500" step="15" ng-model="formData.count_portion" ng-change="onChange()" required="required" readonly>',
        link: function () {
            $('.number-styler').styler();
            $('.jq-number__spin').on('click', function () {
                var value = parseInt($('.number-styler').val());
                count = value
            })
            setTimeout(function(){
              $('.number-styler').val(15);
            },100)
        },
    }
});

app.directive('selectStyler', function () {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            $(element).styler();
            setTimeout(function () {
                $(element).trigger('refresh');
            }, 200);



            attrs.$set('type', null, false);
        },
        priority: 500,
        // compile: function(el, attrs) {

        // }
    }
})

app.directive('select2Styler', function () {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            $(element).select2();

            attrs.$set('type', null, false);
        },
        priority: 500,
        // compile: function(el, attrs) {

        // }
    }
})


app.directive('sliderRangeUi', function () {
    return {
        link: function (scope, element, attrs) {
            $(element).slider({
                range: "max",
                min: parseInt(attrs.min),
                max: parseInt(attrs.max),
                value: parseInt(attrs.value),
                slide: function (event, ui) {
                    scope.$apply(function () {
                        scope[attrs.ngModel] = ui.value;
                        console.log(scope[attrs.ngModel]);
                    });
                    $(this).find('.number-slider').html(ui.value);
                    setTimeout(function () {
                        var pos = $(element[0]).find('.ui-slider-handle').attr('style').split(':');
                        pos = pos[1].slice(0, -1);
                        $(element[0]).find('.number-slider').css({
                            'left': pos
                        })
                    }, 50);
                },
                create: function (event, ui) {
                    scope[attrs.ngModel] = parseInt(attrs.value);
                    $(this).find('.number-slider').html(attrs.value);
                    setTimeout(function () {
                        var pos = $(element[0]).find('.ui-slider-handle').attr('style').split(':');
                        pos = pos[1].slice(0, -1);
                        $(element[0]).find('.number-slider').css({
                            'left': pos
                        })
                    }, 50);
                }
            });
        }
    }
})


// app.factory('getProfile', function($http){
//     return {
//         getProfiles: function() {
//             return $http.get('content.json');
//         }
//     };
// });

app.filter('comma2decimal', [
	function() {
		return function(input) {
        var ret=(input)?input.toString().trim().replace(",","").trim():null;
			return parseFloat(ret);
		};
	}
]);
