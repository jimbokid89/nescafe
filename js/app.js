"use strict";

var app = angular.module('App', ['ngRoute', 'angular.filter']);

app.controller('articlesController', function($scope, $http) {
    $scope.articlesList = [];
    // $scope.quantity = 2;
    //Получение списка статьей
    //пока стоит заглушка в виде JSON

    $http.get('articles.json')
        .success(function(data) {
            $scope.articlesList = data;
            $scope.sorting();
        })
        .error(function(data, status) {
            console.log(data);
        });

    $scope.showMore = function(category) {
        category.quantity += 2;
    };

    $scope.sortingArr = [];

    //Сортировка по категориям - так как мы получаем ВСЕ статьи скопом -> нужно их как то сортировать
    //По факут - обычный цикл в цикле. ищем совпадения по названию категорий. если категорий нету - создаем новую

    $scope.sorting = function() {
        $scope.sortingArr.push({
            'categoryName': $scope.articlesList[0].category,
            'valueArr': [],
            'quantity': 2
        });
        console.log($scope.articlesList.length);
        for (var num = 0; num < $scope.articlesList.length; num++) {
            var tempName = $scope.articlesList[num].category;
            var tempObj = $scope.articlesList[num];
            $.each($scope.sortingArr, function(key, value) {
                var newElem = true;
                // debugger;
                if (tempName === value.categoryName) {
                    // debugger;
                    value.valueArr.push(tempObj);
                    newElem = false;
                    return false;
                }
                if ((value.categoryName != tempName) && (newElem) && ($scope.sortingArr.length - 1) == key) {
                    // debugger;
                    $scope.sortingArr.push({
                        'categoryName': tempName,
                        'valueArr': [tempObj],
                        'quantity': 2
                    });
                    return false;
                }
            });
        }
        console.log($scope.sortingArr);
    };

    //Переход на "деталку статьи"  реализован через localStorage
    //тут при клике на ссылку статьи мы записываем Id в сторедж
    $scope.localStor = function(index) {
        var article = index;
        console.log(index);
        localStorage.setItem('nescafeArticle', index);
    };
});

app.controller('articleController', function($scope, $http) {
    $scope.article = [];

    $scope.articleId = localStorage.getItem('nescafeArticle');

    //Деталка статьи
    //Опять же получаем список ВСЕХ статьей. и дергаем ту что нам нужно по ID

    $http.get('articles.json')
        .success(function(data) {

            $scope.article = data;
            console.log($scope.article);

            $.each($scope.article, function() {
                if (this.id == $scope.articleId) {
                    $('.article-wrapper').html(this.content);
                    history.replaceState(null, null, 'articles/' + this.page_url);
                }
            });


        })
        .error(function(data, status) {
            console.log(data);
        });

        //Показываем статью путем вставки куска HTML прямо в DOM
});


app.controller('calcController', function($scope, $http) {
    //TODO отрефакторить === удалить весь ненужный мусор ниже
    $scope.showState = 'step1';
    $scope.tabState = 'people';
    $scope.activeRow = true;
    $scope.targetTable = {};
    $scope.customPrice = 0;
    $scope.drinksCount = 0;
    $scope.peopleCoef = 2.5;
    $scope.modifier = 0;
    $scope.tableSize = 0;
    $scope.sliderValue = 0;
    $scope.sliderValuePeople = 0;
    $scope.sliderValueBudget = 0;
    $scope.profileName = '';
    $scope.profiles = [];
    $scope.enabledCheck = false;
    $scope.hotelStars = 3;

    $scope.tableChk = function(drinkCheck) {

        // TODO:
        //по условия можно выбрать не менее чем 2 напитика, и обязательно - американо / эспрессо/ капучино - на эти напитки условие прописано непосредственно в html в инпуте
        //

        if ($('.calculation-table .table-row.enabled').length < 3) {
            console.log($('.table-row.enabled').length);
            console.log('выключить');
            $scope.enabledCheck = true;
        } else {
            console.log('включить');
            $scope.enabledCheck = false;
        }
        //TODO ниже кусок стаорого функционала - потом удалить - скрываем кнопку далее если убрана галки с всех напитков
        $scope.disableNextBtn = ($('.table-row.disabled').length === $('.table-row').length) ? true : false;
        $scope.activeRow = true;
    };

    //Два цикла простовляют все галки если юзер зашел на первый шаг после второго
    //

    $scope.setChecks = function() {
        $('.table-row .check-row input').prop('checked', true);
        $.each($scope.hotelPlaceTable.hotelPlaces, function(key, value) {
            value.status = true;
        });
        $.each($scope.targetTable.drinks, function(key, value) {
            value.status = true;
            $scope.modifier = 0;
        });
        $scope.activeRow = true;
    };
    //Класс(конструктор) для создание напитка
    $scope.DrinkClass = function(name, coef, price, cost_price) {
        return {
            drinkName: name,
            drinkCoef: coef,
            drinkPrice: price,
            customPrice: price,
            cost_price: cost_price,
            status: true,
        }
    };

    //************************************************************
    //Перечень машин с всей нужной инфой

    //$scope.DrinkClass('Черный кофе', 0.32, 50, 9),
    // 0.32  - коефициент, 50 - продажа, 9 - себестоимость

    $scope.machine_na_4_30 = {
        'name': 'NESCAFÉ® Alegria A 4/30',
        'maxDrinks': 20,
        'img': 'calcNescafeAlegria4.30.jpg',
        'hotelImg': 'NescafeAlegria4.30.jpg',
        'drinks': [
            $scope.DrinkClass('Черный кофе', 0.25, 50, 9),
            $scope.DrinkClass('Эспрессо', 0.18, 65, 8.3),
            $scope.DrinkClass('Капучино', 0.46, 70, 21.7),
            $scope.DrinkClass('Кофе с молоком', 0.12, 50, 13),
        ],
        'budget': 5500,

    };

    $scope.machine_na_6_30 = {
        'name': 'NESCAFÉ® Alegria 6/30',
        'maxDrinks': 30,
        'img': 'calcNescafeAlegria6.30.jpg',
        'hotelImg': 'NescafeAlegria6.30.jpg',
        'drinks': [
            $scope.DrinkClass('Американо', 0.16, 50, 9),
            $scope.DrinkClass('Эспрессо', 0.29, 45, 8.3),
            $scope.DrinkClass('Капучино', 0.36, 70, 21.7),
            $scope.DrinkClass('Кофе с молоком', 0.16, 50, 13),
            $scope.DrinkClass('Шоколад', 0.11, 60, 13.1),
            $scope.DrinkClass('Мокачино', 0.07, 60, 16.6),
        ],
        'budget': 8500,
        hotelPrice: [{
            name: 'Американо',
            price: 7.6
        }, {
            name: '	Кофе с молоком',
            price: 12.86
        }]
    };

    $scope.machine_na_8_40 = {
        'name': 'NESCAFÉ® Alegria 8/40',
        'maxDrinks': 40,
        'img': 'calcNescafeAlegria8.40.jpg',
        'hotelImg': 'NescafeAlegria8.40.jpg',
        'drinks': [
            $scope.DrinkClass('Американо', 0.125, 50, 9),
            $scope.DrinkClass('Эспрессо', 0.41, 45, 8.3),
            $scope.DrinkClass('Капучино', 0.26, 70, 21.7),
            $scope.DrinkClass('Большой Капучино', 0.20, 70, 21.7),
            $scope.DrinkClass('Кофе с молоком', 0.14, 50, 13),
            $scope.DrinkClass('Шоколад', 0.04, 60, 13.1),
            $scope.DrinkClass('Мокачино', 0.14, 60, 16.6),
            $scope.DrinkClass('Ванильный Капучино', 0.13, 70, 14.5),
        ],
        'budget': 11000
    };

    $scope.machine_na_a_1060 = {
        'name': 'NESCAFÉ® Alegria A 10/60',
        'maxDrinks': 150,
        'img': 'calcNescafeAlegria10.60.jpg',
        'hotelImg': 'NescafeAlegria10.60.jpg',
        'drinks': [
            $scope.DrinkClass('Американо', 0.1, 50, 17.5),
            $scope.DrinkClass('Эспрессо', 0.2, 45, 12.5),
            $scope.DrinkClass('Шоколад', 0.07, 60, 13.1),
            $scope.DrinkClass('Лимонный чай', 0.13, 45, 3.2),
            $scope.DrinkClass('Капучино', 0.14, 70, 21.7),
            $scope.DrinkClass('Ванильный Капучино', 0.16, 70, 14.5),
            $scope.DrinkClass('Кофе с молоком', 0.35, 50, 13),
            $scope.DrinkClass('Мокачино', 0.2, 60, 16.6),
            $scope.DrinkClass('Большой Капучино', 0.1, 80, 28.5),
            $scope.DrinkClass('Большой Американо', 0.1, 70, 18.7),
        ],
        'budget': 41000,
        hotelPrice: [{
            name: 'Американо',
            price: 11.7
        }, {
            name: '		Капучино',
            price: 18.84
        }]
    };

    $scope.machine_na_milano = {
        'name': 'NESCAFÉ® Milano 2',
        'maxDrinks': 100,
        'img': 'calcNescafeMilano.jpg',
        'hotelImg': 'NescafeMilano2.jpg',
        'drinks': [
            $scope.DrinkClass('Американо', 0.2, 50, 9.5),
            $scope.DrinkClass('Эспрессо', 0.1, 45, 8.6),
            $scope.DrinkClass('Шоколад', 0.3, 60, 15.4),
            $scope.DrinkClass('Капучино', 0.4, 70, 21.9),
            $scope.DrinkClass('Латте', 0.2, 80, 24.5),
        ],
        'budget': 28000,
        hotelPrice: [{
            name: 'Американо',
            price: 14.01
        }, {
            name: '	Капучино',
            price: 18.84
        }]
    };

    $scope.machine_na_lounge = {
        'name': 'NESCAFÉ® Milano Lounge',
        'maxDrinks': 100,
        'hotelImg': 'NescafeLounge.jpg',
        'drinks': [
            $scope.DrinkClass('Американо', 0.2, 50, 9.5),
            $scope.DrinkClass('Эспрессо', 0.1, 45, 8.6),
            $scope.DrinkClass('Шоколад', 0.3, 60, 15.4),
            $scope.DrinkClass('Капучино', 0.4, 70, 21.9),
            $scope.DrinkClass('Латте', 0.2, 80, 24.5),
        ],
        'budget': 28000
    };

    $scope.machine_a_gemma = {
        'name': 'Astoria Gemma',
        'maxDrinks': 100,
        'img': 'calcAstoriaGemma.jpg',
        'hotelImg': 'NescafeAlegria10.60.jpg',
        'drinks': [
            $scope.DrinkClass('Американо', 0.15, 50, 17.5),
            $scope.DrinkClass('Эспрессо', 0.25, 45, 12.5),
            $scope.DrinkClass('Капучино', 0.07, 70, 21.7),
            $scope.DrinkClass('Латте', 0.30, 80, 13.22),
            $scope.DrinkClass('Большой Латте', 0.19, 100, 13.22),
            $scope.DrinkClass('Большой Капучино', 0.20, 80, 28.5),
        ],
        'budget': 40000
    };

    $scope.machine_a_calypso = {
        'name': 'Astoria Calypso',
        'maxDrinks': 100,
        'img': 'calcAstoriaCalypso.jpg',
        'hotelImg': 'NescafeAlegriaCalypso.jpg',
        'drinks': [
            $scope.DrinkClass('Американо', 0.20, 50, 17.5),
            $scope.DrinkClass('Эспрессо', 0.15, 45, 12.5),
            $scope.DrinkClass('Капучино', 0.29, 70, 21.7),
            $scope.DrinkClass('Латте', 0.13, 80, 13.22),
            $scope.DrinkClass('Большой Латте', 0.30, 100, 13.22),
            $scope.DrinkClass('Большой Капучино', 0.10, 80, 28.5),
        ],
        'budget': 40000,
        hotelPrice: [{
            name: '	Эспрессо',
            price: 14.15
        }]
    };

    $scope.createHotelPlaceTable = function(zavtrakMachine, lobbyMachine, conferenceMachine, personalMachine) {
        $scope.hotelPlaceTable = {
            hotelPlaces: {
                zavtrak: {
                    'placeName': 'завтрак/шведский стол',
                    'fullPlaceName': 'Ваши затраты на кофе для завтрака/шведского стола',
                    'machine': zavtrakMachine,
                    'status': true,
                    'expenses': true,
                },
                lobby: {
                    'placeName': 'лобби-бар',
                    'fullPlaceName': 'Ваши доходы от продажи кофе в лобби-бара',
                    'machine': lobbyMachine,
                    'status': true,
                    'expenses': false,
                },
                conference: {
                    'placeName': 'конференц-зал',
                    'fullPlaceName': 'Ваши доходы от продажи кофе в конференц-зале',
                    'machine': conferenceMachine,
                    'status': true,
                    'expenses': false,
                },
                personal: {
                    'placeName': 'комната персонала',
                    'fullPlaceName': 'Ваши затраты на кофе для комнаты персонала',
                    'machine': personalMachine,
                    'status': true,
                    'expenses': true,
                }
            }
        }
    };

    //Функция которая дергается после перехода на второй шаг. собвственно сюда и передаем машину по критериям

    $scope.createTargetTable = function(machineName) {
        $scope.targetTable = null;
        $scope.targetTable = machineName;
    };

    $scope.getTotal = function(targetVal) {
        $scope.total = 0;
        $scope.total_marginal = 0;
        $scope.cups = 0;
        $scope.targetTable.drinks.drinksCount = 0;
        $scope.tableSize = $scope.targetTable.drinks.length;
        var countDrinks = [];
        $('.table-row').each(function() {
            countDrinks.push(parseInt($(this).find('.count-drinks .enabled').html()));
        });
        for (var i = 0; i < $scope.targetTable.drinks.length; i++) {
            if ($scope.targetTable.drinks[i].status != false) {
                console.log($scope.targetTable.drinks[i]);
                var row = parseInt($scope.targetTable.drinks[i].customPrice) * countDrinks[i];
                var marginal = parseInt($scope.targetTable.drinks[i].cost_price) * countDrinks[i];
                $scope.total += row;
                $scope.total_marginal += marginal;
                $scope.cups++;
                console.log(row);
            };
        }
        console.log($scope.cups);
        console.log('_____________');
        console.log($scope.total);
        console.log($scope.total_marginal);
    };

    //Страница отеля - функция remove - нажатие на стрелку - ease =)

    $scope.remove = function(item) {
        item.status = !item.status;
    };

    //TODO удалить

    $scope.hotelBtn = 'следующий шаг';

    $scope.nextStep = function() {
        if ($scope.showState == 'step1') {
            //TODO удалить
            $scope.hotelBtn = 'следующий шаг';
            //Подбор таблицы и машины

            //есть пару критериев выбора - бюджет. количество сотрудников и т.д
            //вчисляем какой у нас сейчас выбор -  и дальше выбираем машинку,
            //TODO рефакторинг

            if ($scope.tabState == 'people') {
                $scope.sliderValue = $scope.sliderValuePeople;
                switch (true) {
                    case ($scope.sliderValue <= 20):
                        $scope.createTargetTable($scope.machine_na_4_30)
                        break;
                    case $scope.sliderValue > 20 && $scope.sliderValue <= 30:
                        $scope.createTargetTable($scope.machine_na_6_30)
                        break;
                    case $scope.sliderValue > 30 && $scope.sliderValue <= 40:
                        $scope.createTargetTable($scope.machine_na_8_40)
                        break;
                    case $scope.sliderValue > 40 && $scope.sliderValue <= 150:
                        $scope.createTargetTable($scope.machine_na_a_1060)
                        break;
                }
            } else if ($scope.tabState == 'budget') {
                console.log('budget');
                $scope.sliderValue = $scope.sliderValueBudget;
                switch (true) {
                    case ($scope.sliderValue <= 5500):
                        $scope.createTargetTable($scope.machine_na_4_30)
                        break;
                    case $scope.sliderValue > 5500 && $scope.sliderValue <= 8500:
                        $scope.createTargetTable($scope.machine_na_6_30)
                        break;
                    case $scope.sliderValue > 8500 && $scope.sliderValue <= 11000:
                        $scope.createTargetTable($scope.machine_na_8_40)
                        break;
                    case $scope.sliderValue > 11000 && $scope.sliderValue <= 41000:
                        $scope.createTargetTable($scope.machine_na_a_1060)
                        break;
                }
            } else if ($scope.tabState == 'cafe') {
                switch (true) {
                    case ($scope.sliderValue <= 20):
                        $scope.createTargetTable($scope.machine_na_4_30)
                        break;
                    case $scope.sliderValue > 20 && $scope.sliderValue <= 30:
                        $scope.createTargetTable($scope.machine_na_6_30)
                        break;
                    case $scope.sliderValue > 30 && $scope.sliderValue <= 40:
                        $scope.createTargetTable($scope.machine_na_8_40)
                        break;
                    case $scope.sliderValue > 40 && $scope.sliderValue <= 150:
                        $scope.createTargetTable($scope.machine_na_a_1060)
                        break;
                }
            } else if ($scope.tabState == 'hotel') {
                switch (true) {
                    //$scope.createHotelPlaceTable - создаем обьект с перечнем машин под кажое направление,
                    // !!! порядок передачи аргументов -    Шведская линия, Лобби-бар/ресторан,Конференции,	Питание для персонала
                    case ($scope.hotelStars >= 1 && $scope.hotelStars <= 2):
                        $scope.createHotelPlaceTable($scope.machine_na_6_30, $scope.machine_na_a_1060, $scope.machine_na_6_30, $scope.machine_na_6_30);
                        break;
                    case ($scope.hotelStars >= 3 && $scope.hotelStars <= 4):
                        $scope.createHotelPlaceTable($scope.machine_na_a_1060, $scope.machine_a_calypso, $scope.machine_na_6_30, $scope.machine_na_6_30);
                        break;
                    case ($scope.hotelStars == 5):
                        $scope.createHotelPlaceTable($scope.machine_na_milano, $scope.machine_a_calypso, $scope.machine_na_milano, $scope.machine_na_6_30);
                        break;
                }
            }

            $scope.showState = 'step2';

            //при переходе на второй шаг - нам нужно получить ID направления на каком мы сейчас находимся
            $scope.getProfileMainId();

            setTimeout(function() {
                activeRows = $('.table-row').length;
                if (activeRows <= 4) {
                    $scope.enabledCheck = true;
                } else {
                    $scope.enabledCheck = false;
                }
                $('#checks').val($scope.enabledCheck);
                $('#checks').trigger('input');
                console.log($scope.enabledCheck);
            }, 300);


        } else if ($scope.showState == 'step2') {

            if ($scope.tabState == 'people') {
                $scope.getTotal($scope.sliderValue);
            } else if ($scope.tabState == 'budget') {
                $scope.getTotal($scope.targetTable.maxDrinks);
            } else if ($scope.tabState == 'hotel') {
                console.log('hotel');
            } else if ($scope.tabState == 'cafe') {
                $scope.getTotal($scope.sliderValue);
            }

            // $('.offer-img').attr('src', '/frontend/web/' + $('.hidden-img').attr('ng-src'));

            $scope.showState = 'step3';
            $scope.hotelBtn = 'Оформить заказ';

            $('.hotel-offer-row').each(function() {
                var src = $(this).find('.machine-img .hidden-img').attr('ng-src');
                $(this).find('.machine-img .offer-img').attr('src', '/frontend/web/' + src);
            });
        } else if ($scope.showState == 'step3') {
            $('.calc-form').submit();
        }
    };

    $scope.prevStep = function() {
        if ($scope.showState == 'step3') {
            $scope.showState = 'step2';
            $scope.hotelBtn = 'перейти к следующиму шагу';
        } else if ($scope.showState == 'step2') {
            $scope.showState = 'step1';
            $scope.setChecks();
            $scope.disableNextBtn = false;
            $scope.activeRow = true;
            $scope.enabledCheck = false;

            $scope.hotelBtn = 'перейти к следующиму шагу';
        } else if ($scope.showState == 'step1') {
            location.href = '../site/find-solution';
        }

    };

    $scope.formDataCalcOffer = {
        "profile_id": 0,
        "country": "",
        "city": "",
        "firstname": "",
        "lastname": "",
        "phone": "",
        "email": ""
    };


    $scope.getProfileMainId = function() {
        function getProfilesFn() {
            $http.get('/api/profiles/index')
                .success(function(data) {
                    $scope.profiles = data;
                    console.log($scope.profiles);
                    getProfileIdFn();
                })
                .error(function(data, status) {
                    console.log(data);
                });
        }

        getProfilesFn();

        function getProfileIdFn() {
            $.each($scope.profiles, function(key, value) {
                if (value.name == $scope.profileName) {
                    $scope.formDataCalcOffer.profile_id = value.id;
                }
            });
        }
    };


    $scope.getCalcValues = function() {

        console.log('Call Function!');
        $scope.profiles = [];


        $scope.formDataCalcOffer.machine = $scope.targetTable.name;
        console.log($scope.targetTable.drinks);
        var string = '';
        var cups = [];
        $('.table-row').each(function() {
            cups.push($(this).find('.count-drinks span:last').html());
        });

        console.log(cups);

        $.each($scope.targetTable.drinks, function(value, key) {
            if (key.status === true || key.status === 'true') {
                string += 'Напиток - ' + key.drinkName + ', рекомендуемая цена продажи - ' + key.drinkPrice + ', цена продажи - ' + key.customPrice + ', количество чашек - ' + cups[value] + '; \n';
            }
        });

        $scope.formDataCalcOffer.calculation_option = $('.slider-range-wrap .guests').html() + ': ' + $scope.sliderValue;
        $scope.formDataCalcOffer.expenses = 0;
        $scope.formDataCalcOffer.income = $scope.cups * $scope.total * 31;

        $scope.formDataCalcOffer.additional_information = string;

        console.log($scope.formDataCalcOffer);
    };

    $scope.show = function() {
        console.log($scope.enabledCheck);
    };

    $scope.sendCalcOffer = function() {
        if ($('.calc-form').valid()) {

            $scope.formDataCalcOffer.country = $('.calc-form select[name=country]').val();

            $scope.formDataCalcOffer.city = $('.calc-form select[name=town]').val();

            $scope.getCalcValues();
            preloader('show');
            console.log($scope.formDataCalcOffer);
            $http({
                    method: 'POST',
                    url: '/api/offer/create',
                    data: $scope.formDataCalcOffer
                })
                .then(function(response) {
                    console.log(response);
                    setTimeout(function() {
                        preloader('hide');
                        showPopUpMsg('good');
                    }, 1000);

                })
                .catch(function(err) {
                    console.log(err);

                    setTimeout(function() {
                        preloader('hide');
                        showPopUpMsg('error');
                    }, 1000);
                });
        }
    };
});

var count = 200;

app.controller('offerController', function($scope, $http) {
    $scope.profiles = [];
    $http.get('/api/profiles/index')
        .success(function(data) {
            $scope.profiles = data;
            setTimeout(function() {
                $('select[name=profile]').trigger('refresh');
            }, 100);
        })
        .error(function(data, status) {
            console.log(data);
        });
    // Получения профилей через фабрику, если не нужно можно потом удалить

    // $scope.profiles = [];
    //
    // var handleSuccess = function(data, status) {
    //     $scope.profiles = data;
    //     console.log($scope.profiles);
    // };
    //
    // getProfile.getProfiles().success(handleSuccess);


    $scope.formData = {
        "profile": "",
        "country": "",
        "city": "",
        "count_portion": count,
        "firstname": "",
        "lastname": "",
        "phone": "",
        "email": ""
    };
    $scope.sendOffer = function() {
        $scope.formData.profile = $('.offer-form select[name=profile]').val();
        $scope.formData.city = $('.offer-form select[name=town]').val();
        $scope.formData.count_portion = count;
        if ($('.offer-form').valid()) {
            preloader('show');
            console.log($scope.formData);
            $http({
                    method: 'POST',
                    url: '/api/order/create',
                    data: $scope.formData
                })
                .then(function(response) {
                    console.log(response);
                    setTimeout(function() {
                        preloader('hide');
                        showPopUpMsg('good');
                    }, 1000);

                })
                .catch(function(err) {
                    console.log(err);

                    setTimeout(function() {
                        preloader('hide');
                        showPopUpMsg('error');
                    }, 1000);
                });
        }
    };
});


app.directive('myCustomer', function() {
    return {
        scope: true,
        template: '<input type="number" class="number-styler" min="5" max="500" step="15" ng-model="formData.count_portion" ng-change="onChange()" required="required" readonly>',
        link: function() {
            $('.number-styler').styler();
            $('.jq-number__spin').on('click', function() {
                var value = parseInt($('.number-styler').val());
                count = value;
            });
            setTimeout(function() {
                $('.number-styler').val(5);
            }, 100);
        },
    };
});

app.directive('selectStyler', function() {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            $(element).styler();
            setTimeout(function() {
                $(element).trigger('refresh');
            }, 200);

            attrs.$set('type', null, false);
        },
        priority: 500,
    };
});

app.directive('select2Styler', function() {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            $(element).select2();

            attrs.$set('type', null, false);
        },
        priority: 500,
    };
});


app.directive('sliderRangeUi', function() {
    return {
        link: function(scope, element, attrs) {
            $(element).slider({
                range: "max",
                min: parseInt(attrs.min),
                max: parseInt(attrs.max),
                value: parseInt(attrs.value),
                slide: function(event, ui) {
                    scope.$apply(function() {
                        scope[attrs.ngModel] = ui.value;
                        console.log(scope[attrs.ngModel]);
                    });
                    $(this).find('.number-slider').html(ui.value);
                    setTimeout(function() {
                        var pos = $(element[0]).find('.ui-slider-handle').attr('style').split(':');
                        pos = pos[1].slice(0, -1);
                        $(element[0]).find('.number-slider').css({
                            'left': pos
                        });
                    }, 50);
                },
                create: function(event, ui) {
                    scope[attrs.ngModel] = parseInt(attrs.value);
                    $(this).find('.number-slider').html(attrs.value);
                    setTimeout(function() {
                        var pos = $(element[0]).find('.ui-slider-handle').attr('style').split(':');
                        pos = pos[1].slice(0, -1);
                        $(element[0]).find('.number-slider').css({
                            'left': pos
                        });
                    }, 50);
                }
            });
        }
    };
});

app.filter('comma2decimal', [
    function() {
        return function(input) {
            var ret = (input) ? input.toString().trim().replace(",", "").trim() : null;
            return parseFloat(ret);
        };
    }
]);

app.filter('customDate', [
    function() {
        return function(input) {
            var ret = input.toString().split(" ");
            return (ret[0]);
        };
    }
]);


//TODO убрать этот весь мусор ниже
//Прелоудер + поп-ап с инфой об отправки формы

function preloader(status) {
    if (status == 'show') {
        $('.preloader').fadeIn();
    } else if (status == 'hide') {
        $('.preloader').fadeOut();
        closePop()
    }
}

function showPopUpMsg(response) {
    var popMsg = $('.pop-up.message');
    popMsg.fadeIn();
    if (response == 'good') {
        popMsg.find('.pop-msg').html('Ваша заявка отправлена');

    } else if (response == 'error') {
        popMsg.find('.pop-msg').html('Произошла ошибка');
    }
    popMsg.css({
        'top': $(window).scrollTop() + 100
    })
}
