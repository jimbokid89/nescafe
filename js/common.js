'use strict';
if (!window.console) window.console = {};
if (!window.console.memory) window.console.memory = function() {};
if (!window.console.debug) window.console.debug = function() {};
if (!window.console.error) window.console.error = function() {};
if (!window.console.info) window.console.info = function() {};
if (!window.console.log) window.console.log = function() {};


// sticky footer

if (!Modernizr.flexbox) {
    (function() {
        var
            $pageWrapper = $('#page-wrapper'),
            $pageBody = $('#page-body'),
            noFlexboxStickyFooter = function() {
                $pageBody.height('auto');
                if ($pageBody.height() + $('#header').outerHeight() + $('#footer').outerHeight() < $(window).height()) {
                    $pageBody.height($(window).height() - $('#header').outerHeight() - $('#footer').outerHeight());
                } else {
                    $pageWrapper.height('auto');
                }
            };
        $(window).on('load resize', noFlexboxStickyFooter);
    })();
}


if (ieDetector.ieVersion == 10 || ieDetector.ieVersion == 11) {
    (function() {
        var
            $pageWrapper = $('#page-wrapper'),
            $pageBody = $('#page-body'),
            ieFlexboxFix = function() {
                if ($pageBody.addClass('flex-none').height() + $('#header').outerHeight() + $('#footer').outerHeight() < $(window).height()) {
                    $pageWrapper.height($(window).height());
                    $pageBody.removeClass('flex-none');
                } else {
                    $pageWrapper.height('auto');
                }
            };
        ieFlexboxFix();
        $(window).on('load resize', ieFlexboxFix);
    })();
}
//Global
var fpDestroyed = false;
var openDetail = false;

//-----------------------------------------------------------------------------
$(function() {

    // placeholder
    //-----------------------------------------------------------------------------
    $('input[placeholder], textarea[placeholder]').placeholder();

    //Full Page https://github.com/alvarotrigo/fullPage.js

    $('#fullpage').fullpage({
        lockAnchors: false,
        navigation: true,
        navigationPosition: 'left',
        showActiveTooltip: true,
        slidesNavigation: true,
        slidesNavPosition: 'bottom',
        scrollBar: true,
        verticalCentered: true,
        responsiveHeight: 600,
        onLeave: function(index, nextIndex, direction) {
            if (nextIndex === 2 || nextIndex === 3 || nextIndex === 4 || nextIndex === 6) {
                $('#fp-nav').addClass('gold')
            } else {
                $('#fp-nav').removeClass('gold')
            }
        },
    });


    $('.js-scroll').on('click', function(e) {
        e.preventDefault();
        $('#fullpage').fullpage.moveTo(2)
    })

    //Slick Slider - http://kenwheeler.github.io/slick/

    $('.solutions-slider').slick({
        slidesToShow: 3,
        arrows: false,
        dots: false,
        speed: 500,
        infinite: true,
        centerMode: true,
        focusOnSelect: true,
        touchMove: false,
        easing: 'easeInOutCubic'
    })
    $('.js-slider-prev').on('click', function(e) {
        e.preventDefault()
        $('.solutions-slider').slick('slickPrev');
    });

    $('.js-slider-next').on('click', function(e) {
        e.preventDefault()
        $('.solutions-slider').slick('slickNext');
    });

    $('.advantages-items').on('mouseover', function() {
        $(this).addClass('fill-start');
    });

    $('.slider-footer').slick({
        slidesToShow: 1,
        arrows: false,
        speed: 1800,
        dots: true,
        touchMove: false,
        touchThreshold: false,
        autoplay: true,
        autoplaySpeed: 2000,
        fade: true,
        easing: 'easeInOutCubic'
    })

    $('.slider .item').each(function(e) {
        $(this).attr('id', e);
        $(this).css({
            'z-index': 99
        })
    });

    setTimeout(function() {
        $('.slider .item:first').addClass('current')
        slider();
    }, 200)

    var slides = $('.slider .item').length

    var duration = 4500;
    var autoSlide;
    var buttons = 0;

    function slider() {
        console.log('slideInit');
        if ($('.item.current').length > 0) {
            autoSlide = setInterval(function() {
                $('.item.current').css({
                    'z-index': 99
                });

                var currentId = parseInt($('.item.current').attr('id'));
                var nextId = currentId + 1;
                var buttons = currentId + 2;

                if (currentId == (slides - 1)) {
                    nextId = 0;
                    buttons = 1;
                }
                $('.buttons ul li a.active').removeClass('active');
                $('.buttons ul li:nth-child(' + buttons + ') a').addClass('active');

                $('#' + nextId).css({
                    'z-index': 100
                }).addClass('animate')

                setTimeout(function() {
                    $('.item.current').removeClass('current');
                    $('.item.animate').removeClass('animate').addClass('current');
                }, 1000)
            }, duration);
        }
    }


    $('.js-goto-slide').on('click', function(e) {
            e.preventDefault();
            $('.item.animate').removeClass('animate')
            $('.buttons ul li a.active').removeClass('active');
            clearInterval(autoSlide)
            setTimeout(slider(), 100);
            var id = $(this).attr('href')

            $('.item').css({
                'z-index': 99
            })
            $(id).css({
                'z-index': 101
            });

            $(id).addClass('animate');

            setTimeout(function() {
                $('.item.current').removeClass('current');
                $(id).removeClass('animate').addClass('current');
            }, 1000)

            $(this).addClass('active')
        })
        //Catalog page fns's
        //Catalog category btn
    $('.js-show-category').on('click', function(e) {
        e.preventDefault()
        if ($(this).hasClass('active')) {
            $('.catalog-items').removeClass('active')
            $('.catalog-menu').removeClass('show')
        } else {
            $('.catalog-items').addClass('active')
            $('.catalog-menu').addClass('show')
        }
        $(this).toggleClass('active')
    })

    $('.matchHeight').matchHeight({
        property: 'height',
    });

    $('.js-scroll-top').on('click', function(e) {
        e.preventDefault()
        $("body, html").animate({
            scrollTop: 0
        }, 1200);
    })

    //Detail page sliders
    $('.slider-for').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        asNavFor: '.slider-nav'
    });
    $('.slider-nav').slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        asNavFor: '.slider-for',
        dots: true,
        focusOnSelect: true
    });
    $('.other-interesting-slider').slick({
            slidesToShow: 4,
            slidesToScroll: 4,
            dots: true,
            arrows: true,
        })
        // Validation Form detail page https://jqueryvalidation.org/
    jQuery.validator.addMethod("cyrillic", function(value, element) {
        return this.optional(element) || /^[\-А-ЯЁа-яёії]{1,30}$/.test(value);
    }, 'Please enter a cyrillic');

    $('.offer-form').validate({
        ignore: ".number-styler",
        rules: {
            town: {
                required: true,
            },
            profile: {
                required: true,
            },
            firstName: {
                required: true,
                cyrillic: true
            },
            lastName: {
                required: true,
                cyrillic: true
            },
            phone: {
                required: true,
                number: true,
            },
            email: {
                required: true,
                email: true
            },
            rules: {
                required: true,
            }
        },
        messages: {
            town: {
                required: 'Введите город',
            },
            profile: {
                required: 'Выберете профиль',
            },
            firstName: {
                required: 'Введите имя',
                cyrillic: 'Пожалуйста, укажи свое имя. Допускаются только буквы кириллицы и дефис'
            },
            lastName: {
                required: 'Введите фамилию',
                required: 'Пожалуйста, укажи свою фамилию. Допускаются только буквы кириллицы и дефис',
            },
            phone: {
                required: 'Введите номер телефона',
                number: 'В номере телефона должны быть только цифри',
            },
            email: {
                required: 'Введите email',
                email: 'Введите валидный email'
            },
            rules: {
                required: 'Вы должны согласится',
            }
        },
        submitHandler: function(form) {
            return false;
        }
    })


    $('.calc-form').validate({
        rules: {
            town: {
                required: true,
            },
            country: {
                required: true,
            },
            firstName: {
                required: true,
                cyrillic: true,
            },
            lastName: {
                required: true,
                cyrillic: true,
            },
            phone: {
                required: true,
                number: true,
            },
            email: {
                required: true,
                email: true
            },
            rules: {
                required: true,
            }
        },
        messages: {
            town: {
                required: 'Введите город',
            },
            country: {
                required: 'Выберете страну',
            },
            firstName: {
                required: 'Введите имя',
                cyrillic: 'Пожалуйста, укажи свое имя. Допускаются только буквы кириллицы и дефис'
            },
            lastName: {
                required: 'Введите фамилию',
                cyrillic: 'Пожалуйста, укажи свое имя. Допускаются только буквы кириллицы и дефис'
            },
            phone: {
                required: 'Введите номер телефона',
                number: 'В номере телефона должны быть только цифри',
            },
            email: {
                required: 'Введите email',
                email: 'Введите валидный email'
            },
            rules: {
                required: 'Вы должны согласится',
            }
        },
        submitHandler: function(form) {
            return false;
        }

    })

    $('.js-ingridients-show-more').on('click', function(e) {
        e.preventDefault();
        $('.ingridient-info').fadeOut();
        $("body, html").animate({
            scrollTop: 0
        }, 1200);
        $('.pop-up-offer').css('top', 0);
    })

    $('.js-show-pop-up').on('click', function(e) {
        e.preventDefault();



        var dataPop = $(this).attr('data-pop-up');
        var videoSrc = $(this).attr('href');


        switch (dataPop) {
            case 'pop-up-offer':
                closePop();
                break;
            case 'video':
                $('.pop-up').css({
                    'top': $(window).scrollTop() + 50
                });
                $('.pop-up.video').fadeIn('400').prepend('<iframe width="560" height="315" src="' + videoSrc + '" frameborder="0" allowfullscreen></iframe>');
                break;
            case 'callback':
                $('.pop-up.' + dataPop + '').slideDown(600).addClass('active');
                $('.pop-up.callback').css({
                    'top': ($(window).outerWidth() < 1500 ? 118 : 152)
                })
                break;
            case 'ingridient-info':
                var itemTarget = $(this).attr('data-info');
                $('.pop-up.' + dataPop + '').fadeIn('400').addClass('active');
                $('.pop-up.' + dataPop + '').css({
                    'top': $(window).scrollTop() + 50
                });
                $('.' + itemTarget).show();
                break;



        }

        if (dataPop != 'callback') {
            overlay.fadeIn('400', function() {
                $('.pop-up.' + dataPop + '').fadeIn('400').addClass('active');
            });
        };

        if ($('.page-wrapper').hasClass('about-new') || $('.page-wrapper').hasClass('main-page')) {
            if (!(fpDestroyed)) {
                openDetail = !openDetail;
                fpDestroyed = !fpDestroyed;
                $('#about-fullpage').fullpage.setAutoScrolling(false);
                $('#about-fullpage').fullpage.setFitToSection(false);
            }
        };
    });

    $('.js-info-grafic').on('click', function(e) {
        e.preventDefault();

        if ($(this).hasClass('active')) {
            $(this).html('Узнать больше');
            $('.about-coffee').removeClass('noMargin')
            $('.main-articles-wrap').slideUp();
        } else {
            $(this).html('Свернуть');
            $('.about-coffee').addClass('noMargin')
            $('.main-articles-wrap').slideDown();
            $('html, body').animate({
                scrollTop: $($.attr(this, 'href')).offset().top
            }, 1000);
        }

        $(this).toggleClass('active');
    })
    $("#amount").val($("#slider-range-max").slider("value"));
});

$(window).on('resize load', function() {
    var windowHeight = $(window).outerHeight() < 650 ? 600 : $(window).outerHeight();
    $('.js-full-height').css('height', windowHeight);
})
$(window).on('resize', function() {
    var changeValue = 0;
    checkWindow()
})
$(window).on('load', function() {
    if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1) {
        if ($(window).height() > 880) {
            $('.img-side').css({
                'background-size': '100%'
            })
        }
    }
    var sliderWidth = function() {
        if ($(window).outerWidth() < 767) {
            return 767;
        } else {
            return ($(window).outerWidth() + 20);
        }
    }();
    // var sliderWidth = ($(window).outerWidth() + 20 ) || ( ($(window).outerWidth() < 767 ) ? 767);
    if ((sliderWidth % 2) == 1) {
        $('.slider').css({
            'width': sliderWidth + 1,
            'position': 'relative',
            'left': '1px'
        })
    }
    checkWindow(sliderWidth)
})


function checkWindow(width) {

    if ($(window).outerWidth() < 1360 && $(window).outerWidth() > 768) {
        var sliderWidth = $(window).width() + 40;
    } else if ($(window).outerWidth() < 768) {
        var sliderWidth = width;
    }

    if ((sliderWidth % 2) == 1) {
        $('.slider').css({
            'width': sliderWidth + 1,
            'position': 'relative',
            'left': '1px'
        })
    } else {
        $('.slider').css({
            'width': sliderWidth,
            'position': 'relative',
            'left': '1px'
        })
    }
    if ($(window).width() / 2 >= $(window).height()) {
        var width = $(window).width() / 2;
        if ($(window).width() % 2 == 1) {
            width = ($(window).width() + 1) / 2
        }

        $('.header .main-image .half img').css({
            'width': width + 50,
            'height': 'initial',
        })
    } else {
        $('.header .main-image .half img').css({
            'width': 'initial',
            'height': '100%'
        })
    }

    if ($(window).height() > 900) {
        $('.header .main-image .half img').css({
            'height': '100%',
        })
    }


    //////////////////////////////////////

    var machine = $('img.machine');
    var moveLen = $(window).outerWidth() / 25

    if ($(window).outerWidth() > 1500) {
        machine.css({
            'left': -(moveLen - 25),
        })
    } else {
        machine.css({
            'left': -(moveLen - 10),
        })
    }

    if ($(window).outerWidth() <= 1025) {
        machine.css({
            'left': -(moveLen + 33.6),
        })
    }

    //////////////////////////////////////

    setTimeout(function() {
        if ($(window).outerHeight() < 600) {
            $('#fullpage').fullpage.setAutoScrolling(false)
            $('.section').addClass('fixed');
        } else {
            $('#fullpage').fullpage.setAutoScrolling(true)
            $('.section').removeClass('fixed');
        }
    }, 1000)

    var margin = 0;

    if ($(window).outerWidth() < 1360) {
        margin = 9;
    } else {
        margin = 0;
    }

    if ($(window).outerWidth() < 997) {
        margin = 15;
    }

    var logoPos = $('.logo').offset().left;
    $('#fp-nav').css({
        'left': logoPos + margin
    })


}

var overlay = $('.overlay');
var pop = $('.pop-up');

function closePop() {
    overlay.fadeOut();
    if (pop.hasClass('video active')) {
        pop.fadeOut('400', function() {
            pop.find('iframe').remove()
        })
    }
    setTimeout(function() {
        pop.find('.content-pop-up').hide()
    }, 400);
    pop.removeClass('active');
    pop.fadeOut('');
    $('.form-wrapper ').find('input[type=text],input[type=tel],input[type=email]').val('');

    if ($('.page-wrapper').hasClass('main-page')) {
        fpDestroyed = !fpDestroyed;


        $('html, body').animate({
            scrollTop: $('.section.active').offset().top
        }, 500);

        setTimeout(function() {
            $('#fullpage').fullpage.setAutoScrolling(true);
            $('#fullpage').fullpage.setFitToSection(true);
        }, 500);

    }
}

$(document).on('click', '.close-pop-up,.overlay,.js-close-pop-up', function(e) {
    e.preventDefault();
    closePop()
})


if ($('.page-wrapper.articles-main ').length != 0) {
    var image = $('.bean-animation');
    var imageSecond = $('.bean-animation-2');
    var imageThird = $('.bean-animation-3');

    var modifier = 8;

    $(window).on('scroll', function() {

        if ($(window).outerWidth() < 1500 || $(window).outerHeight() < 800) {

            if ($(window).scrollTop() > ($('.js-full-height').height() + 100) && $(window).scrollTop() < 1119) {
                console.log('first');
                image.css({
                    'transform': 'translateY(' + ($(window).scrollTop() - $('.growth').offset().top) + 'px)'
                })
            }

            if ($(window).scrollTop() > 1020 && $(window).scrollTop() < 1337) {
                console.log('second');

                imageSecond.css({
                    'transform': 'translateY(' + ($(window).scrollTop() - ($('section.selection').offset().top - 650)) + 'px)'
                })
            }
            if ($(window).scrollTop() > 1937 && $(window).scrollTop() < 3337) {
                imageThird.css({
                    'transform': 'translateY(' + ($(window).scrollTop() - 1700) + 'px)'
                })
            }

        } else {

            if ($(window).scrollTop() > ($('.js-full-height').height() * 0.9) && $(window).scrollTop() < 1400) {
                image.css({
                    'transform': 'translateY(' + ($(window).scrollTop() - $('.growth').offset().top + 150) + 'px)'
                })
            }

            if ($(window).scrollTop() > 1200 && $(window).scrollTop() < 1700) {
                imageSecond.css({
                    'transform': 'translateY(' + ($(window).scrollTop() - $('.roasting').offset().top / 2) + 'px)'
                })
            }

            if ($(window).scrollTop() > 2500 && $(window).scrollTop() < 4600) {
                imageThird.css({
                    'transform': 'translateY(' + ($(window).scrollTop() - $('.roasting').offset().top) + 'px)'
                })
            }
        }
    });
}




var used = 0;
var changeNum = 1;
var activeRows = 0;
var modifierSum = 0;
var modifierScope = 0;
var disableNextBtn = false;
var checks = false;


$(document).on('click', 'input[type=checkbox]', function() {
    var test = 0;
    var lenght = $('.table-row').length;

    $('.table-row.disabled').each(function() {
        test += parseInt($(this).closest('.table-row').find('.count-drinks span:first').html());
    })
    activeRows = lenght - $('.table-row.disabled').length;

    console.log(activeRows);

    $('.table-row.disabled').length;
    modifierScope = Math.round(test / activeRows);
    $('#Test').val(modifierScope)
    $('#Test').trigger('input');
});
