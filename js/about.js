'use strict';

var fpDestroyed = false;
$(function () {
    $('.about-slider').slick({
        slidesToShow: 1,
        arrows: false,
        speed: 500,
        dots: true,
        touchMove: false,
        touchThreshold: false,
        autoplay: true,
        autoplaySpeed: 2000,
        easing: 'easeInOutCubic'
    })

    $('#about-fullpage').fullpage({
        lockAnchors: false,
        scrollBar: true,
        verticalCentered: true,
        autoScrolling: true,
        resize : false,
        easing: 'easeOutCubic',
        scrollingSpeed: 700,
    });




    $('.js-show-detail').on('click', function (e) {
        e.preventDefault();
        $(this).closest('.section-parent').css({
            'height': 'auto'
        })
        var target = $(this).attr('href');

        if ($(this).hasClass('active')){
            $(target).slideUp();
            $(this).html('Узнать больше');
            $(this).removeClass('active');
            return
        } else {
            $(target).insertAfter($(this).closest('.section'));
            $(target).slideDown();
            $(this).html('Свернуть');

        }
        var windowHeight = $(window).outerHeight() < 350 ? 550 : $(window).outerHeight();
        if (!(fpDestroyed)){
            fpDestroyed = !fpDestroyed;
            $('#about-fullpage').fullpage.setAutoScrolling(false);
            $('#about-fullpage').fullpage.setFitToSection(false);
        }
        if (!(openDetail)) {
            openDetail = !openDetail;
        }
        setTimeout(function(){
            console.log('slideFn1');
            slideTo(target)
        },500)
        $(this).addClass('active')
    })

    function slideTo(targetLink) {
        console.log('slideFn');
        $('html, body').animate({
            scrollTop: $(targetLink).offset().top
        }, 500);
    }

    //mouse move about page

    var offsetX = 0;
    var offsetY = 0;

    var realOffsetX = 0;
    var realOffsetY = 0;

    var width = $(window).outerWidth()
    var height = $(window).outerHeight()

    var multiply = 15;
    $('.js-img').css({
        'transform': 'scale(1.15)'
    })

    $('.section').mousemove(function( event ) {
        var img = $(this).find('.js-img');
        realOffsetX = 0.5 - event.offsetX / width;
        realOffsetY = 0.5 - event.offsetY / height;
        offsetX += (realOffsetX - offsetX) * 0.1;
        offsetY += (realOffsetY - offsetY) * 0.1;
        img.css({
            'transform': 'scale(1.15) translateY(' + -offsetX + 'px) rotateX(' + -offsetY * multiply + 'deg) rotateY(' + offsetX * -multiply + 'deg) '
        })
    });
})



$(window).on('load resize', function () {
    aspectRatio()
    aboutFullHeight()

})

function aboutFullHeight(){
    var windowHeight = $(window).outerHeight() < 620 ? 600 : $(window).outerHeight();
    if ($('.about-new .section').outerHeight() <= 620){
        console.log(windowHeight)
        $('#about-fullpage').fullpage.destroy('all');
        fpDestroyed = !fpDestroyed;
        $('.footer').addClass('static');
        $('.about-new .section').css('height', windowHeight);
    }
}

function aspectRatio(){
    //Aspect ratio About page
    if ($(window).outerWidth() / $(window).outerHeight() < 1.8 ){
        $('.aspect-ratio').addClass('height').removeClass('width')
    } else {
        $('.aspect-ratio').addClass('width').removeClass('height')
    }
}
