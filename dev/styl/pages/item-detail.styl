
.item-detail-header
  height 152px
  background #832a39
  @media (max-width $screen-md-max)
      height: 118px;
.item-detail-wrap
  position: relative
  .half-side
    width: 50%
    float: left
    height: 1050px
    @media (max-width 1500px)
        height: auto;
.item-view
  background #f2f2f2
  .slider-for
    max-width: 507px;
    max-height: 507px;
    margin: 79px auto -8px;
    @media (max-width 1500px)
        max-width @max-width * 0.7
        max-height @max-height * 0.7
        margin: 45px auto 0px
  .slider-nav
    max-width: 792px;
    margin  0 auto
    .nav-items
      padding 0 25px 50px
      position: relative
      opacity: 0.5
      &.slick-current
        opacity: 1
        &:before
          content ''
          width: 77 px
          height: 4px
          background $darkGold
          position: absolute
          left: -3px;
          right: 0;
          bottom: 59px;
          margin auto
.item-info
  padding-top: 69px;
  padding-left: 162px;
  padding-right: 5%;
  @media (max-width 1500px)
      padding-left 106px
      padding-top 41px
  @media (max-width 1100px)
      padding-left: 5%;
  .bread-crumbs
    margin-bottom: 55px;
    @media (max-width 1500px)
        margin-bottom 36px
    ul
      list-style-type: none
      li
        &:not(:last-child)
          &:after
            content '>'
            display: inline-block
            color $darkGold
        display: inline-block
        a,span
          color $darkGold
  .item-name
    font-family:proximaNovaBold;
    font-size: 30px
    line-height: 30px
    color #303032
    letter-spacing: -0.3px;
    margin-bottom: 23px;
    @media (max-width 1500px)
        font-size 18px
        line-height 18px
        margin-bottom 13px
  .item-info-detail
    font-family:proximaNovaLight;
    font-size: 21px
    line-height: 31px
    color: #2a2a2a
    margin-bottom: 75px;
    position: relative

    &:before
        content ''
        height: 1px
        width: 415px
        background #e9eaea
        position: absolute
        bottom: -46px;
        left: 0
    @media (max-width 1500px)
        font-size 13px
        line-height 18px
        margin-bottom 45px
        &:before
            bottom: -25px;
            width 50%

  .characteristics
    margin-bottom: 69px;
    @media (max-width 1500px)
        margin-bottom 41px
    ul
      list-style-type: none
      li
        padding-left: 58px;
        font-family:proximaNovaLight;
        color #2a2a2a
        font-size: 21px
        line-height: 55px
        position: relative
        letter-spacing: 1px;
        @media (max-width 1500px)
            padding-left 35px
            font-size 13px
            line-height 33px
        b
          font-family:proximaNovaSemiBold;
        &:before
          content ''
          display: inline-block
          position: absolute

        &.cups
            &:before
                background url(../img/cups-icon.png) no-repeat;
                width: 36px
                height: 25px
                top: 17px;
                left: -6px;
            @media (max-width 1500px)
                &:before
                    top 0
        &.water
            &:before
                background url(../img/water-icon.png) no-repeat;
                width: 23px
                height: 28px
                top: 15px;
                left: -2px;
            @media (max-width 1500px)
                &:before
                    top 0
        &.sizes
            &:before
                background url(../img/sizes-icon.png) no-repeat;
                width: 26px
                height: 26px
                top: 15px;
                left: -4px;
            @media (max-width 1500px)
                &:before
                    top 3px
        &.weight
            &:before
                background url(../img/weight-icon.png) no-repeat;
                width: 27px
                height: 23px
                top: 15px;
                left: -4px;
            @media (max-width 1500px)
                &:before
                    top 0px
        &.powered
            &:before
                background url(../img/powered-icon.png) no-repeat;
                width: 26px
                height: 19px
                top: 15px;
                left: -4px;
            @media (max-width 1500px)
                &:before
                    top 0
        &.power
            &:before
                background url(../img/power-icon.png) no-repeat;
                width: 22px
                height: 25px
                top: 12px;
                left: -2px;
            @media (max-width 1500px)
                &:before
                    top 0
  .offer-btn
    border-color $darkGold
    color: #676767
    margin-bottom: 79px;
    min-width: 321px;
    &:hover
      color: #fff
      border-color: $darkRed

.social-sharing
    position: relative
    &:before
        content ''
        height: 1px
        width: 415px
        background #e9eaea
        position: absolute
        top: -23px;
        left: 0
    @media (max-width 1500px)
        &:before
            bottom: -25px;
            width 50%
    span
        font-family:proximaNovaThin;
        font-size: 12px
        text-transform: uppercase
        letter-spacing: 1.5px;
        margin-right: 25px;
    a
        display: inline-block
        margin-right: 8px;
.other-slider
  max-width: 1280px
  margin: 159px auto 310px;
  @media (max-width 1500px)
     max-width: 80%
     margin: 95px auto 186px;
  .slider-title
    text-align: center
    font-family:proximaNovaBold;
    font-size: 22px
    color #333743
    text-transform: uppercase
    letter-spacing: 1px;
    margin-bottom: 46px;
    position: relative
    @media (max-width 1500px)
        font-size 14px
        margin-bottom 27px
  .other-item
    @media (max-width 1500px)
        padding 15px
    &-name
      font-family:proximaNovaRegular;
      font-size: 20px
      margin-top: 14px;
      margin-bottom: 9px;
      letter-spacing: -0.1px;
      color #333743
      @media (max-width 1500px)
          font-size 13px
    &-category
      font-family:proximaNovaRegular;
      font-size: 15px
      color: #9297a3
      @media (max-width 1500px)
          font-size 13px
    .price
      float: right
      font-family:proximaNovaRegular;
      font-size: 17px
      color: #9297a3
      margin-right: 17px;
      margin-top: 15px;
      @media (max-width 1500px)
          font-size 13px
    .newest
      position: absolute;
      display: block;
      width: 131px;
      height: 45px;
      text-align: center;
      text-transform: uppercase;
      color: #fff;
      background: $darkGold;
      top: 42%;
      left: 2px;
      letter-spacing: 1px;
      transform: translateY(-56%);
      padding-top: 12px;
      @media (max-width 1500px)
          font-size 12px
          width 78px
          height 28px
          padding-top 6px
  .slick-dots
    bottom: -84px;
    li
      margin: 0 1px;
      button
        border 1px solid $darkGold
        width: 9 px
        height: 9px
        border-radius 50%
        background $darkGold
        padding: 0;
        &:before
          display: none
      &.slick-active
        button
          background transparent
  .slick-prev, .slick-next
    top: 44%
    @media (max-width 1500px)
        top 35%
  .slick-prev
    left: -85px;
    @media (max-width 1500px)
        left -50px
    &:before
      content ''
      background url(../img/item-slider-prev.png) no-repeat;
      width: 18 px
      height: 36px
      display: block
      position: relative
  .slick-next
    right: -85px
    @media (max-width 1500px)
        right -50px
    &:before
      content ''
      background url(../img/item-slider-next.png) no-repeat;
      width: 18 px
      height: 36px
      display: block
      position: relative

.show-video
    font-size: 23px
    line-height: 29px
    color: #303032
    position: relative
    padding-left: 95px;
    display: block
    text-transform: uppercase
    width: 315px;
    margin: 0 auto 55px;
    letter-spacing: 0.5px;
    span
        width: 76 px
        height: 76px
        border 1px solid #ded4c4
        border-radius 50%
        display: inline-block
        font-family:proximaNovaBold;
        font-size: 22px
        position: absolute
        top: -11px;
        left: 0
        text-align: center
        padding-top: 24px;
        @media (max-width 1500px)
          width @width  * 0.66
          height: @height * 0.66
          font-size: @font-size * 0.66
          padding-top: 18px;
    b
        display: block
        font-family:proximaNovaBold;
    @media (max-width 1500px)
      font-size: @font-size * 0.66
      line-height: 16px;
      width @width  * 0.66
      padding-left: 57px;


.pop-up
    position absolute
    z-index 150
    display none
    .close-pop-up
        position absolute
        right: 34px;
        top: 38px;
        background: url(../img/close-btn.png) no-repeat
        display inline-block
        width 19px
        height 19px
        &:hover
            opacity: .5
            transition: .2s ease
.ingridient-info
  display none
  left 0
  right 0
  margin: auto;
  width 1280px
  min-height 60px
  background: #fff;
  z-index: 150;
  @media (max-width: 1500px)
    width @width *0.6697
  @media (max-width $screen-md-max)
    width 90%
    min-width 685px
  .content-pop-up
    display none

.pop-up-offer
    position: absolute
    width: 46%;
    right:0
    top 0
    background #fff
    z-index: 150
    padding-top: 67px;
    padding-bottom: 92px;
    transform: translateX(100%);
    transition: .5s ease
    &.active
        transform: translateX(0%);
        transition: .5s ease

    .form-wrapper
        width 370px
        margin: 0 auto
    .pop-up-title
        font-size 24px
        line-height 30px
        font-family: proximaNovaBold
        color: #303032
        text-transform uppercase
        margin-bottom: 30px;
        @media (max-width 1500px)
            font-size 15px
            line-height 18px
            margin-bottom 18px


    .pop-up-sub-title
        font-size 17px
        line-height 18px
        font-family: proximaNovaRegular
        color: #606060
        margin-bottom: 26px;
        padding-left: 1px;
        letter-spacing: 0.85px;
        @media (max-width 1500px)
            font-size 12px
            line-height 14px
            text-align justify
            br
              display: none
        b,a
            font-family: proximaNovaBold
            color: #606060
            text-decoration none
    .form-group
        position relative

        label
            display block
            font-family: proximaNovaRegular
            font-size 16px
            line-height 20px
            text-transform uppercase
            color: #606060
            letter-spacing: 0.8px;
            margin-bottom: 15px;
            @media (max-width 1500px)
                font-size 12px
                margin-bottom 9px
            b
                // font-size 24px
                color: #832a39

        .number-wrapper
            border: 1px solid #c2c2c2
            width 121px
            height 56px

            .styler-wrapper
              height 100%
            @media (max-width 1500px)
                height 34px
            .jq-number
                padding 0
                height: 100%;
                width: 100%;
                .jq-number__field
                    width 50%
                    border: none
                    box-shadow: none
                    margin: 0 auto;
                    margin-top: 6px;
                    @media (max-width 1500px)
                        margin-top: -4px;
                    input
                        font-family: proximaNovaLight
                        font-size 21px
                        text-align center
            .jq-number__spin
                box-shadow: none
                border-radius: 0
                width 18px
                height 18px
                border: 1px solid #c2c2c2
                background: #fff
                top: 50%
                transform: translateY(-50%);
                text-shadow: none
                &:hover
                    background: #b6a47d
                    border-color: #b6a47d
                    &:after
                        color: #fff
                &.minus,&.plus
                    &:after
                        border none
                        left 0
                        top: -7px;
                        font-size 21px
                        font-family: proximaNovaLight
                        text-align center
                        width 100%
            .jq-number__spin.minus
                left 13px
                &:after
                    content: '-'

            .jq-number__spin.plus
                right 13px
                &:after
                    content: '+'
                    top: -6px;
.rules
    margin-top: -15px;
    margin-bottom: 38px;
    font-size 12px
    font-family: proximaNovaRegular
    color: #606060
    b
        color: #832a39
.form-group
    margin-bottom: 32px;
    @media (max-width 1500px)
        margin-bottom 20px
.checkbox-rule
    position relative
    margin-bottom: 60px;
    >input
        opacity 0
        position absolute
        z-index -1
    label.checkbox-view
        width 18px
        height 19px
        border: 1px solid #dedede
        position relative
        cursor: pointer;
        display inline-block
        z-index 1
        margin-bottom 0
    >input:checked ~ .checkbox-view
        &:before
            content: ''
            position absolute
            top 50%
            left 0
            right 0
            margin: auto;
            transform: translateY(-50%);
            background url(../img/checkMark.png) no-repeat;
            width 12px
            height 10px
    span
        position: absolute;
        left: 29px;
        font-size 12px
        line-height 15px
        color: #606060
.input-style
    width 100%
    height 56px
    border: 1px solid #c2c2c2
    font-size: 16px;
    font-family: proximaNovaRegular;
    padding-left 20px
    @media (max-width 1500px)
        height 34px
        padding-left 10px
    &.error
        border: 1px solid red



.submit-form-btn
    width 100%
    height 58px
    background: #832a39
    color: #fff;
    border: none
    text-transform uppercase
    font-family: proximaNovaBold
    font-size 15px
    position relative
    padding-top: 5px;
    letter-spacing: 3px;
    text-align: left;
    padding-left: 17.1%;
    cursor: pointer;
    &:before
        content: ''
        position absolute
        top 51%
        transform: translateY(-50%);
        right 21%
        background: url(../img/shoping-cart-icon-white.png)no-repeat
        width 23px
        height 20px
    @media (max-width 1500px)
        height 40px
        font-size 12px


.overlay
    position fixed
    top 0
    left 0
    background: #000
    z-index: 140
    opacity 0.87
    width 100%
    height 100%
    display none
.pop-up.video
    display none
    left 0
    right 0
    margin: auto;
    padding 60px 15px 15px
    background: #fff
    width 600px
    z-index 150
    .close-pop-up
        right: 20px;
        top: 20px;


.styler
    width 100%
    &.error
      .jq-selectbox__select
        border-color: red
.styler .jq-selectbox__select
    width 100%
    height 56px
    border: 1px solid #c2c2c2
    border-radius: 0
    box-shadow: none
    background: #fff
    padding-left: 16px;
    @media (max-width 1500px)
        height 34px

    .jq-selectbox__select-text
        line-height 56px
        @media (max-width 1500px)
            line-height 34px
    .placeholder
        font-size 16px
        font-family: proximaNovaRegular
        color: #606060
        line-height: 55px;
        letter-spacing: 1px;
        width: 100% !important;
        @media (max-width 1500px)
            line-height  34px
            font-size 12px
    .jq-selectbox__trigger
        border: none
        .jq-selectbox__trigger-arrow
            top 47%
            right 23px
            border-top: 8px solid #999;
            border-right: 7px solid transparent;
            border-left: 7px solid transparent;
            @media (max-width 1500px)
                top: 43%;
                right: 12px;
select.error ~ .jq-selectbox__select
    border: 1px solid
.form-group
  label.error
      color: red !important
      display inline-block !important
      margin-bottom 0 !important
      font-size 12px !important

  &.town-select,&.checkbox-rule
      label.error
          position absolute
          bottom: -22px;
  &.custom-error-position
    label.error
        position absolute
        bottom: -22px;
.town-select
  .select2-container
    width 100%  !important
    .select2-selection--single
      height 54px
      border-radius: 0;
      @media (max-width: 1500px)
        height 34px
      .select2-selection__arrow
        height 100%
        b
          border-width: 8px 7px 0 7px;
          margin-left: -16px;
          margin-top: -3px;
          left 0
          @media (max-width 1500px)
            margin-left: -5px;
      .select2-selection__rendered
        padding-left: 18px;
        line-height 54px
        @media (max-width: 1500px)
          line-height: 33px;

      .select2-selection__placeholder
        font-family proximaNovaRegular
        font-size 16px
        line-height 54px
        color: #606060;
        letter-spacing: 1px;
        @media (max-width: 1500px)
          font-size 12px
          line-height 34px
.select2-container--default.select2-container--open
  .select2-selection--single
    .select2-selection__arrow
      b
        border-width: 8px 7px 0 7px;


#profile-error{
    position: absolute;
    bottom: -22px;
}

select.error ~ .jq-selectbox__select,
select.error ~ .select2-container--default .select2-selection--single
{
    border: 1px solid red;
}
